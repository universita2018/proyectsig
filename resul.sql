-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-12-2018 a las 01:40:00
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resul`
--

CREATE TABLE `resul` (
  `id_result` int(11) NOT NULL,
  `resultado` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `resul`
--

INSERT INTO `resul` (`id_result`, `resultado`) VALUES
(1, '0'),
(2, '0'),
(3, '0'),
(4, '0'),
(5, '0'),
(6, '0'),
(7, '0'),
(8, '0'),
(9, '0'),
(10, '0'),
(11, '0'),
(12, '0'),
(13, '0'),
(14, '0'),
(15, '(-17.4081948323057, -66.01316018776197)'),
(16, '(-17.40802978563267, -66.01449053003671)'),
(17, '(-17.408265246562014, -66.01363758756997)'),
(18, '(-17.409283867521914, -66.01423303797128)'),
(19, '(-17.409263392784684, -66.01387898638131)'),
(20, '(-17.409217324617483, -66.01346592619302)'),
(21, '(-17.409074001356405, -66.01320843412759)'),
(22, '(-17.40913542562493, -66.01297239973428)'),
(23, '(-17.40912518824828, -66.01271490766885)'),
(24, '(-17.40787110527004, -66.01465443815323)');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `resul`
--
ALTER TABLE `resul`
  ADD PRIMARY KEY (`id_result`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `resul`
--
ALTER TABLE `resul`
  MODIFY `id_result` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
